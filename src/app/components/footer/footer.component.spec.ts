import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
	let component: FooterComponent;
	let fixture: ComponentFixture<FooterComponent>;

	beforeEach(
		async(() => {
			TestBed.configureTestingModule({
				declarations: [ FooterComponent ]
			}).compileComponents();
		})
	);

	beforeEach(() => {
		fixture = TestBed.createComponent(FooterComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should render user email link', () => {
		fixture = TestBed.createComponent(FooterComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector('div.email a').href).toEqual('mailto:katarzyna.knapik@gmail.com');
	});

	it('should render user linkedin link', () => {
		fixture = TestBed.createComponent(FooterComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector('div.linkedin a').href).toEqual('https://www.linkedin.com/in/katarzynaknapik/');
	});

	it('should render user gitlab link', () => {
		fixture = TestBed.createComponent(FooterComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector('div.gitlab a').href).toEqual('https://gitlab.com/katarzyna.knapik/');
	});
});
