// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB_ym8ABc5Pt5S62ZCNo-PlwSBpKN3gd1w",
    authDomain: "tonal-history-312617.firebaseapp.com",
    projectId: "tonal-history-312617",
    storageBucket: "tonal-history-312617.appspot.com",
    messagingSenderId: "711509687615",
    appId: "1:711509687615:web:c37c3ed11b38bc92d9e78a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
